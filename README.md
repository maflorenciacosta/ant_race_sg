## Stadium Goods | Ants Challenge

This project is a custom solution for [Mobile App | Hiring Challenge](https://stadiumgoodshq.notion.site/Mobile-App-Hiring-Challenge-82bc55b799f84b19af6960eb96b51309).

## Components

In this project you will find `/src/components/` directory where all the UI components are placed.

## Available Scripts

In the project directory, you can run at least:

### `npm run ios`

### `npm run android`
