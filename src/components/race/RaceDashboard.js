/**
 * @module RaceDashboard
 * @description view that will render the dashboard where we can start the race
 */

import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Text} from 'react-native';
import {StartRaceButton} from './StartRaceButton';
import {AntsList} from '../ants/AntsList';
import {COLORS, FONT_SIZES} from '../../views/styles';
import {isRaceFinished} from '../../utils/raceStatus';

export function RaceDashboard({ants = [], onRaceStart}) {
  const [isStarted, setIsStarted] = useState(false);
  const isFinished = isRaceFinished(ants);

  const handleStart = () => {
    setIsStarted(true);
    onRaceStart();
  };

  return (
    <View style={raceDashboardStyles.container}>
      <Text
        style={[
          raceDashboardStyles.raceStatusText,
          isStarted && isFinished && raceDashboardStyles.fetchDataText,
        ]}>
        Race: {getRaceStatusText(isStarted, isFinished)}
      </Text>

      {!isFinished && <StartRaceButton onStart={handleStart} />}

      <AntsList ants={ants} isStarted={isStarted} />
    </View>
  );
}

RaceDashboard.propTypes = {
  ants: PropTypes.arrayOf(
    PropTypes.shape({
      likelihood: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      length: PropTypes.number.isRequired,
      color: PropTypes.string.isRequired,
      weight: PropTypes.number.isRequired,
    }),
  ).isRequired,
  onRaceStart: PropTypes.func.isRequired,
};

const raceDashboardStyles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    height: '100%',
  },
  raceStatusText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: FONT_SIZES.heading2,
    color: COLORS.color1,
    marginBottom: 24,
    textTransform: 'uppercase',
  },
  fetchDataText: {
    color: COLORS.color3,
  },
});

function getRaceStatusText(isStarted, isFinished) {
  if (!isStarted) {
    return 'Not Yet Run';
  }

  if (isStarted && !isFinished) {
    return 'In Progress';
  }

  if (isStarted && isFinished) {
    return 'Calculated!';
  }
}
