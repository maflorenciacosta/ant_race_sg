/**
 * @module StartRaceButton
 * @description component that handles the start of calculations in order to be used in a Raice.
 */

import React, {useState, useCallback} from 'react';
import PropTypes from 'prop-types';
import {Button} from 'react-native';
import {COLORS} from '../../views/styles';
import {useRegressiveCount} from '../../utils/customHooks/useRegressiveCount';

const COUNT_DOWN_SECONDS = 3;

export function StartRaceButton({onStart}) {
  const [isCountingDown, setIsCountingDown] = useState(false);

  // we use useCallback so the `useRegressiveCount` custom hook will not sees this method changing all the time
  const handleStart = useCallback(() => {
    onStart();
    setIsCountingDown(false);
  }, [onStart]);

  const count = useRegressiveCount(
    COUNT_DOWN_SECONDS,
    isCountingDown,
    handleStart,
  );

  const isDisabled = isCountingDown || count === 0;
  const startButtonText = isDisabled
    ? count > 0
      ? `${count}`
      : 'Go!'
    : 'Press to Start a Race';

  return (
    <Button
      onPress={() => setIsCountingDown(true)}
      title={startButtonText}
      accessibilityLabel={startButtonText}
      color={COLORS.color1}
      disabled={isDisabled}
    />
  );
}

StartRaceButton.propTypes = {
  onStart: PropTypes.func.isRequired,
};
