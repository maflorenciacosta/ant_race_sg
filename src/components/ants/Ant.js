/**
 * @module Ant
 * @description module that represents one particular Ant with his own calculation. It will render the details and the position after finish a raice.
 */

import React, {useRef} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Text, ActivityIndicator} from 'react-native';
import {COLORS, FONT_SIZES} from '../../views/styles';
import {NOT_CALCULATED} from '../../utils/raceStatus';

export function Ant({
  index,
  likelihood,
  name,
  length,
  color,
  weight,
  isStarted,
  isFinished,
}) {
  // Storing the initial position for each ant before the race starts.
  // Since I don't want show the updates of the position, I store this info in a reference instead of an state.
  const initialPositionRef = useRef(index + 1);

  return (
    <View style={antStyles.antContainer}>
      <View style={antStyles.ant}>
        <AntStatus
          index={index}
          likelihood={likelihood}
          isStarted={isStarted}
          isFinished={isFinished}
        />

        <View style={[antStyles.ant, antStyles.antText, antStyles.antName]}>
          <Text style={antStyles.antText}>
            {initialPositionRef.current}) {name}
          </Text>
        </View>

        {likelihood !== NOT_CALCULATED && (
          <Text style={antStyles.antDetailText}>
            Likelihood: {likelihood.toFixed(2)}
          </Text>
        )}

        <AntDetails length={length} color={color} weight={weight} />
      </View>
    </View>
  );
}

Ant.propTypes = {
  index: PropTypes.number.isRequired,
  likelihood: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  length: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  weight: PropTypes.number.isRequired,
  isStarted: PropTypes.bool.isRequired,
  isFinished: PropTypes.bool.isRequired,
};

const STATUS = {
  NOT_STARTED: 'Not Yet Run',
  PROGRESS: 'In Progress',
  FINISHED: 'Calculated!',
};

/**
 * @method AntStatus
 * @description Private component created to support <Ant />
 */
function AntStatus({isStarted, isFinished, index, likelihood}) {
  const antStatus = parseAntStatus(likelihood, isStarted);

  return (
    <View style={antStyles.antPositionContainer}>
      {isStarted && isFinished && (
        <Text style={[antStyles.antText, antStyles.antPosition]}>
          {parsePosition(index)}
        </Text>
      )}

      {antStatus === STATUS.PROGRESS && <ActivityIndicator size="small" />}

      <Text
        style={[
          antStyles.antStatusText,
          antStatus === STATUS.FINISHED && antStyles.antFinishedText,
        ]}>
        {antStatus}
      </Text>
    </View>
  );
}

AntStatus.propTypes = {
  index: PropTypes.number.isRequired,
  likelihood: PropTypes.number.isRequired,
  isStarted: PropTypes.bool.isRequired,
  isFinished: PropTypes.bool.isRequired,
};

/**
 * @method AntDetails
 * @description Private component created to support <Ant />
 */
function AntDetails({length, color, weight}) {
  return (
    <View style={antStyles.detailsContainer}>
      <Text style={antStyles.antDetailText}>Length: {length} | </Text>
      <Text style={antStyles.antDetailText}>Color: {color} | </Text>
      <Text style={antStyles.antDetailText}>Weight: {weight}</Text>
    </View>
  );
}

AntDetails.propTypes = {
  length: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  weight: PropTypes.number.isRequired,
};

/**
 * @method parseAntStatus
 * @description calculates the state of each ant's likelihood calculation.
 *
 * @returns an string that represent the status of an Ant.
 */
function parseAntStatus(likelihood, isStarted) {
  if (!isStarted) {
    return STATUS.NOT_STARTED;
  }

  return likelihood === -1 ? STATUS.PROGRESS : STATUS.FINISHED;
}

/**
 * @method parsePosition
 * @description method that calculates the label for each position of an ant.
 */
function parsePosition(position) {
  position = position + 1;

  switch (position) {
    case 1:
      return '1st';
    case 2:
      return '2nd';
    case 3:
      return '3rd';
    default:
      return `${position}th`;
  }
}

const antStyles = StyleSheet.create({
  detailsContainer: {
    flexDirection: 'row',
  },
  antContainer: {
    alignItems: 'center',
    textAlign: 'center',
  },
  ant: {
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 5,
    alignItems: 'center',
  },
  antName: {
    backgroundColor: COLORS.color1,
    borderRadius: 100,
    marginBottom: 12,
  },
  antPositionContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 8,
  },
  antPosition: {
    color: COLORS.base4,
    fontSize: FONT_SIZES.heading2,
    marginBottom: 4,
  },
  antText: {
    color: COLORS.base2,
    fontWeight: 'bold',
  },
  antDetailText: {
    color: COLORS.base3,
    fontWeight: 'bold',
  },
  antStatusText: {
    color: COLORS.color1,
    fontWeight: 'bold',
  },
  antFinishedText: {
    color: COLORS.color5,
  },
});
