/**
 * @module FetchAntsButton
 * @description button that take cares of fetch the Ants' data
 */

import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Button, ActivityIndicator} from 'react-native';
import {COLORS} from '../../views/styles';

export function FetchAntsButton({onPress, isLoading}) {
  return (
    <View style={fetchAntsButtonStyles.container}>
      {isLoading ? (
        <View>
          <ActivityIndicator size="large" />
        </View>
      ) : (
        <Button
          onPress={onPress}
          title="Press to Fetch Ants information"
          color={COLORS.color1}
          accessibilityLabel="Press to Fetch Ants information"
        />
      )}
    </View>
  );
}

FetchAntsButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

const fetchAntsButtonStyles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'center',
  },
});
