/**
 * @module AntsList
 * @description module that render a list of ants
 */

import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, FlatList} from 'react-native';
import {Ant} from './Ant';
import {COLORS} from '../../views/styles';
import {isRaceFinished} from '../../utils/raceStatus.js';

export function AntsList({isStarted, ants}) {
  const isFinished = isRaceFinished(ants);

  const antRender = propsWithoutPropTypes => {
    const {index, item} = propsWithoutPropTypes;

    return (
      <Ant
        {...item}
        key={item.name}
        index={index}
        isStarted={isStarted}
        isFinished={isFinished}
      />
    );
  };

  return (
    <View style={[antsStyles.ants, antsStyles.antsRaceStarted]}>
      <FlatList
        extraData={ants}
        horizontal={false}
        data={ants}
        renderItem={antRender}
        keyExtractor={ant => ant.name}
        ItemSeparatorComponent={() => <View style={antsStyles.antSeparator} />}
      />
    </View>
  );
}

AntsList.propTypes = {
  isStarted: PropTypes.bool.isRequired,
  ants: PropTypes.arrayOf(
    PropTypes.shape({
      likelihood: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      length: PropTypes.number.isRequired,
      color: PropTypes.string.isRequired,
      weight: PropTypes.number.isRequired,
    }),
  ).isRequired,
};

const antsStyles = StyleSheet.create({
  antSeparator: {
    width: '100%',
    borderWidth: 1,
    marginVertical: 4,
    opacity: 0.2,
    borderColor: COLORS.color1,
  },
  ants: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  antsRaceStarted: {
    alignItems: 'center',
    maxHeight: '90%',
  },
});
