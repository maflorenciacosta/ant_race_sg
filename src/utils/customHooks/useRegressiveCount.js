import {useEffect, useState} from 'react';

/**
 * @module useRegressiveCount
 * @description custom hooks that calculate a regressive count depending on a number
 */
export function useRegressiveCount(seconds, isCounting, onCountEnd) {
  const [count, setCount] = useState(seconds);

  useEffect(() => {
    if (!isCounting) return;

    if (count === 0) {
      onCountEnd();
    } else {
      const timerId = setTimeout(() => setCount(count - 1), 1000);
      return () => clearTimeout(timerId);
    }
  }, [count, seconds, isCounting, onCountEnd]);

  return count;
}
