// value of a winning odds of an ants that is not yet calculated
// (it should be lower than any legal odd's value for sorting purposes)
// it should be less than the posible result of the Math.random
export const NOT_CALCULATED = -1;

/**
 * @module isRaceFinished
 * @description method that calculates if the race is finished depending on the ants likelihood calculation
 */
export function isRaceFinished(ants) {
  return ants.every(ant => ant.likelihood !== NOT_CALCULATED);
}
