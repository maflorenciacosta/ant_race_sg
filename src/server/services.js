import {NOT_CALCULATED} from '../utils/raceStatus';

/**
 * @method getAntsData
 * @description load Ants' data and throw an error if there is an issue in the API service call
 *
 * @return {{Promise<object[]>}} an array of ants
 */
export async function getAntsData() {
  const response = await fetch('http://sg-ants-server.herokuapp.com/ants', {
    method: 'GET',
  });

  const {ants} = await response.json();

  return ants.map(ant => ({
    ...ant,
    likelihood: NOT_CALCULATED,
  }));
}
