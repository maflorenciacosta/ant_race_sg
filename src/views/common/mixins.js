import {COLORS} from '../styles';

export const BASE_BUTTON = {
  alignSelf: 'center',
  alignItems: 'center',
  padding: 4,
  borderRadius: 50,
  width: '100%',
  borderColor: COLORS.base2,
  borderWidth: 1,
};
