/**
 * @module styles
 * @description common place where variables related to styling are stored
 */

export const COLORS = {
  base1: '#f2f2f2',
  base2: '#DDDDDD',
  base3: '#b2b2b2',
  base4: '#2c2c2c',

  color1: '#1592e6',
  color2: '#f6c713',
  color3: '#d52221',
  color4: '#05b95f',
  color5: '#5014d6',
  color6: '#983b91',
};

export const FONT_SIZES = {
  heading1: 24,
  heading2: 18,
  heading3: 14,
  body1: 12,
  body2: 10,
};

export const FONT_FAMILIES = {
  font1: 'Helvetica Neue',
};
