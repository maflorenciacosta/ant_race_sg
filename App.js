/**
 * @module App
 * @description main module where the Ants Race will handle the views
 */
import React, {useState} from 'react';
import {SafeAreaView, StyleSheet, View, StatusBar, Text} from 'react-native';
import {RaceDashboard} from './src/components/race/RaceDashboard';
import {FetchAntsButton} from './src/components/ants/FetchAntsButton';
import {COLORS} from './src/views/styles';
import {getAntsData} from './src/server/services';
import {generateAntWinLikelihoodCalculator} from './src/utils/generateAntWinLikelihoodCalculator';

export default function App() {
  const [ants, setAnts] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState();

  const handleFetchAnts = async () => {
    try {
      setLoading(true);
      const data = await getAntsData();
      setAnts(data);
      setLoading(false);
    } catch (e) {
      setError('Error: Unable to get ants list');
    }
  };

  const startRace = () => {
    ants.forEach(ant => {
      // callback to pass down to the likelihood calculator
      function getLikelihood(likelihood) {
        setAnts(oldAnts =>
          oldAnts
            .map(oldAnt =>
              oldAnt.name === ant.name ? {...oldAnt, likelihood} : oldAnt,
            )
            .sort((antA, antB) => antB.likelihood - antA.likelihood),
        );
      }

      // this callback needs to be different for each ant, so it is calculated inside the foreach
      generateAntWinLikelihoodCalculator()(getLikelihood);
    });
  };

  return (
    <>
      <StatusBar hidden />
      <SafeAreaView>
        <View style={appStyles.container}>
          {!error &&
            (ants.length === 0 ? (
              <FetchAntsButton
                onPress={handleFetchAnts}
                isLoading={isLoading}
              />
            ) : (
              <RaceDashboard ants={ants} onRaceStart={startRace} />
            ))}

          {error && <Text style={appStyles.error}>{error}</Text>}
        </View>
      </SafeAreaView>
    </>
  );
}

const appStyles = StyleSheet.create({
  container: {
    height: '100%',
    padding: 28,
  },
  error: {
    color: COLORS.color3,
    textAlign: 'center',
    width: '100%',
    padding: 10,
  },
});
